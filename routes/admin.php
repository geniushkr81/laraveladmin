<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;


Route::get('', [HomeController::class,'index']);
Route::resource('categories', CategoryController::class);
Route::resource('products', ProductController::class);
//Route::get('categories', [CategoryController::class,'create']);