@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Agregar Nuevo Producto</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{route('products.index')}}"> Regresar</a><p>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   <section class="content">
       <div class="container-fluid">
<form action="{{ route('products.store')}}" method="POST">
    @csrf
  <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
                <strong>Categoria:</strong>
      <select name="category" id="category" class="form-control">
  @foreach($categories as $item)
    <option value="{{ $item->name }}">{{ $item->name }}</option>
  @endforeach
        </select>
</div>
</div>
</div>
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Detalles:</strong>
                <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Imagen:</strong>
                    <input type="text" name="image" class="form-control" placeholder="Image URL" />
                    <input type="hidden" name="url" class="form-control" placeholder="Image URL" value="nothing" />
                </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-warning">Guardar Informacion</button>
        </div>
    </div>
   
</form>
</div>
</section>
@endsection