@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>LISTADO DE CATEGORIAS</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success btn-sm" href="{{route('categories.create')}}" > Crear nueva categoria</a>
                <p>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table id="example" class=" display table table-striped table-hover shadow-lg table-sm mt-4">
        <thead style="background-color:orange" class="primary text-white">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Details</th>
            <th>Image</th>
            <th width="180px">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($categories as $categ)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $categ->name }}</td>
            <td>{{ $categ->detail }}</td>
            <td>{{ $categ->image }}</td>
            <td>
                <form id="delete-form" action="{{route('categories.destroy', $categ->id)}}" method="POST">
   
                <a class="btn btn-info btn-sm" href="{{ route('categories.show',$categ->id) }}">Show</a>
                    <a class="btn btn-primary btn-sm" href="{{ route('categories.edit',$categ->id) }}">Edit</a>

   
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm" id="delete-user" onclick="return false" ><i class="fa fa-trash"></i></button>
                    
                    
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {!! $categories->links() !!}
      
   
@endsection

@section('js')
<script>
    $(document).ready(function() {
    $('#example').DataTable({
        dom: 'Bfrtip',
        buttons:['excelHtml5'],
    });
} );
     $('#delete-user').on('click', function (e) {
            e.preventDefault();
            let id = $(this).data('id');
            Swal.fire({
                title: 'Are you sure ?',
                text: "You won't be able to revert this !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value === true) {
                    Swal.fire('Message','','success');
                    console.log('enter....');
                    $('#delete-form').submit();
                }
            })
        });
    </script>
@stop